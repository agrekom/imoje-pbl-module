/*browser:true*/
/*global define*/
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (Component,
              rendererList) {
        'use strict';
        rendererList.push(
            {
                type: 'imoje_pbl',
                component: 'Imoje_Pbl/js/view/payment/method-renderer/pbl'
            }
        );
        return Component.extend({});
    }
);
