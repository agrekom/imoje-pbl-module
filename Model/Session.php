<?php

namespace Imoje\Pbl\Model;

/**
 * Transaction model
 *
 * @method int getLastOrderId()
 * @method Session setLastOrderId(int)
 * @method array getOrderCreateData()
 * @method Session setOrderCreateData(array)
 * @method string getPaytype()
 * @method Session setPaytype(string)
 */
class Session extends \Magento\Framework\Session\SessionManager
{

}
