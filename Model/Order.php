<?php

namespace Imoje\Pbl\Model;

use Magento\Checkout\Model\Session;
use Magento\Checkout\Model\Session\SuccessValidator;
use Magento\Framework\App\RequestInterface;

/**
 * Class Order
 *
 * @package Imoje\Pbl\Model
 */
class Order
{

	/**
	 * @var SuccessValidator
	 */
	protected $checkoutSuccessValidator;

	/**
	 * @var Session
	 */
	protected $checkoutSession;

	/**
	 * @var RequestInterface
	 */
	protected $request;
	/**
	 * @var Sales\Order
	 */
	private $orderFactory;

	/**
	 * @param SuccessValidator               $checkoutSuccessValidator
	 * @param Session                        $checkoutSession
	 * @param RequestInterface               $request
	 * @param Sales\Order|Sales\OrderFactory $orderFactory
	 */
	public function __construct(
		SuccessValidator $checkoutSuccessValidator,
		Session $checkoutSession,
		RequestInterface $request,
		Sales\OrderFactory $orderFactory
	) {
		$this->checkoutSuccessValidator = $checkoutSuccessValidator;
		$this->checkoutSession = $checkoutSession;
		$this->request = $request;
		$this->orderFactory = $orderFactory;
	}

	/**
	 * @param int $orderId
	 *
	 * @return Sales\Order|false
	 */
	public function loadOrderById($orderId)
	{
		/**
		 * @var $order Sales\Order
		 */
		$order = $this->orderFactory->create();
		$order->load($orderId);
		if($order->getId()) {
			return $order;
		}

		return false;
	}

	/**
	 * @return int|false
	 */
	public function getOrderId()
	{
		if($this->checkoutSuccessValidator->isValid()) {
			return $this->checkoutSession->getLastOrderId();
		}
		$orderId = $this->request->getParam('id');
		if($orderId) {
			return $orderId;
		}

		return false;
	}

	/**
	 * @return bool
	 */
	public function paymentSuccessCheck()
	{
		return is_null($this->request->getParam('exception'));
	}
}
