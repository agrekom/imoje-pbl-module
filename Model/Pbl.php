<?php

namespace Imoje\Pbl\Model;

include_once __DIR__ . "/../../imoje-libs-module/PaymentCore/src/Util.php";

use Imoje\Pbl\Model\Payment\AbstractPaymentGateway;
use Imoje\Payment\Api;
use Imoje\Payment\Payment;
use Imoje\Payment\Util;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Asset\Repository as AssetRepository;
use Magento\Payment\Helper\Data;
use Magento\Payment\Model\Method\Logger;
use Magento\Sales\Model\Order;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Sales\Model\Service\CreditmemoService;
use Magento\Sales\Model\Order\CreditmemoFactory;

/**
 * Class Pbl
 *
 * @package Imoje\Pbl\Model
 */
class Pbl extends AbstractPaymentGateway
{
	const CODE = 'imoje_pbl';

	/**
	 * @var UrlInterface
	 */
	protected $urlBuilder;

	/**
	 * @var string
	 */
	protected $code = self::CODE;

	/**
	 * @var Magento\Sales\Model\Order\CreditmemoFactory
	 */
	protected $creditmemoFactory;
	/**
	 * @var Magento\Sales\Model\Service\CreditmemoService
	 */
	protected $creditmemoService;

	/**
	 * @var Order
	 */
	private $order;

	/**
	 * @var StoreManagerInterface
	 */
	private $storeManager;

	/**
	 * @var Payment
	 */
	private $paymentGateway;

	/**
	 * @var AssetRepository
	 */
	private $assetRepository;

	/**
	 * Pbl constructor.
	 *
	 * @param Context                    $context
	 * @param Registry                   $registry
	 * @param ExtensionAttributesFactory $extensionFactory
	 * @param AttributeValueFactory      $customAttributeFactory
	 * @param Data                       $paymentData
	 * @param ScopeConfigInterface       $scopeConfig
	 * @param Logger                     $logger
	 * @param UrlInterface               $urlBuilder
	 * @param Order                      $order
	 * @param StoreManagerInterface      $storeManager
	 * @param AssetRepository            $assetRepository
	 * @param AbstractResource|null      $resource
	 * @param AbstractDb|null            $resourceCollection
	 * @param array                      $data
	 * @param CreditmemoFactory          $creditmemoFactory
	 * @param CreditmemoService          $creditmemoService
	 */
	public function __construct(
		Context $context,
		Registry $registry,
		ExtensionAttributesFactory $extensionFactory,
		AttributeValueFactory $customAttributeFactory,
		Data $paymentData,
		ScopeConfigInterface $scopeConfig,
		Logger $logger,
		UrlInterface $urlBuilder,
		Order $order,
		StoreManagerInterface $storeManager,
		AssetRepository $assetRepository,
		CreditmemoFactory $creditmemoFactory,
		CreditmemoService $creditmemoService,
		AbstractResource $resource = null,
		AbstractDb $resourceCollection = null,
		array $data = []
	) {
		parent::__construct(
			$context,
			$registry,
			$extensionFactory,
			$customAttributeFactory,
			$paymentData,
			$scopeConfig,
			$logger,
			$resource,
			$resourceCollection,
			$data
		);

		$this->assetRepository = $assetRepository;
		$this->urlBuilder = $urlBuilder;
		$this->order = $order;
		$this->storeManager = $storeManager;
		$this->creditmemoFactory = $creditmemoFactory;
		$this->creditmemoService = $creditmemoService;

		$this->setStore($this->storeManager->getStore()->getId());
	}

	/**
	 * @return string
	 */
	public function getCheckoutRedirectUrl()
	{

		return $this->urlBuilder->getUrl('imoje_pbl/payment/checkout');
	}

	/**
	 * @return bool
	 */
	public function canRefund()
	{
		return true;
	}

	/**
	 * @param \Magento\Payment\Model\InfoInterface $payment
	 * @param float                                $amount
	 *
	 * @return bool
	 */
	public function refund(\Magento\Payment\Model\InfoInterface $payment, $amount)
	{

		$api = new Api($this->getConfigData('authorization_token'), $this->getConfigData('merchant_id'), $this->getConfigData('service_id'));

		$order = $payment->getOrder();

		$amount = Util::convertAmountToFractional($amount);

		$refund = $api->createRefund(
			$api->prepareRefundData(
				$amount
			),
			$payment->getParentTransactionId()
		);

		if($refund['success']) {

			$creditmemo = $this->creditmemoFactory->createByOrder($order);
			$creditmemo->setGrandTotal($amount);
			$this->creditmemoService->refund($creditmemo);
			$this->creditmemoService->notify($creditmemo->getId());

			return true;
		}

		throw new \Magento\Framework\Exception\LocalizedException(__('The refund action is not available at this moment. Try again later.'));
	}

	/**
	 * @return string
	 */
	public function getPaymentLogoSrc()
	{

		return $this->getConfigData('hide_brand')
			? ''
			: $this->assetRepository->getUrl('Imoje_Pbl::images/logo.png');
	}

	/**
	 * @param string $currencyCode
	 *
	 * @return bool
	 */
	public function canUseForCurrency($currencyCode)
	{

		$currencies = $this->getConfigData('currency');

		if(!is_string($currencies) || !$currencies) {
			return false;
		}

		return Util::canUseForCurrency(
				strtolower($currencyCode),
				explode(
					',',
					strtolower(
						str_replace(' ', '', $currencies)
					)
				)
			)
			&& $this->getConfigData('authorization_token')
			&& $this->getConfigData('service_id')
			&& $this->getConfigData('service_key')
			&& $this->getConfigData('merchant_id');
	}

	/**
	 * @return mixed
	 */
	public function getLocale()
	{
		return $this->scopeConfig->getValue(
			'general/locale/code',
			\Magento\Store\Model\ScopeInterface::SCOPE_STORE,
			$this->getStore()
		);
	}
}
