<?php

namespace Imoje\Pbl\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Payment\Helper\Data;

/**
 * Class ConfigProvider
 *
 * @package Imoje\Pbl\Model
 */
class ConfigProvider implements ConfigProviderInterface
{
	const PP_CODE = 'imoje_pbl';

	/**
	 * @var \Imoje\Pbl\Model\Pbl
	 */
	protected $pblMethod;

	/**
	 * Payment ConfigProvider constructor.
	 *
	 * @param Data $paymentHelper
	 *
	 * @throws \Magento\Framework\Exception\LocalizedException
	 */
	public function __construct(Data $paymentHelper)
	{
		$this->pblMethod = $paymentHelper->getMethodInstance(self::PP_CODE);
	}

	/**
	 * Retrieve assoc array of checkout configuration
	 *
	 * @return array
	 */
	public function getConfig()
	{
		$config = [];

		if($this->pblMethod->isAvailable()) {
			$config = [
				'payment' => [
					self::PP_CODE => [
						'redirectUrl'    => $this->pblMethod->getCheckoutRedirectUrl(),
						'paymentLogoSrc' => $this->pblMethod->getPaymentLogoSrc(),
					],
				],
			];
		}

		return $config;
	}
}
