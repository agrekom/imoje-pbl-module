<?php

namespace Imoje\Pbl\Block;

include_once __DIR__ . "/../../imoje-libs-module/PaymentCore/autoload.php";

use Imoje\Payment\Api;
use Imoje\Payment\Util;
use Imoje\Pbl\Model\Order;
use Magento\Checkout\Helper\Cart;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\App\ResponseFactory;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\ScopeInterface;
use Zend\Log\Logger;
use Zend\Log\Writer\Stream;

/**
 * Class Checkout
 *
 * @package Imoje\Pbl\Block
 */
class Checkout extends Template
{

	/**
	 * @var UrlInterface
	 */
	public $urlBuilder;

	/**
	 * @var ScopeConfigInterface
	 */
	protected $scopeConfig;

	/**
	 * @var Registry
	 */
	private $registry;

	/**
	 * @var Order
	 */
	private $order;

	/**
	 * @var StoreInterface
	 */
	private $store;

	/**
	 * @var Cart
	 */
	private $cartHelper;

	/**
	 * @var ResponseFactory
	 */
	private $responseFactory;

	/**
	 * @var ProductMetadataInterface $productMetadataInterface
	 */
	private $productMetadataInterface;

	/**
	 * Checkout constructor.
	 *
	 * @param Context                  $context
	 * @param Order                    $order
	 * @param Registry                 $registry
	 * @param StoreInterface           $store
	 * @param UrlInterface             $urlBuilder
	 * @param ScopeConfigInterface     $scopeConfig
	 * @param Cart                     $cartHelper
	 * @param ResponseFactory          $responseFactory
	 * @param ProductMetadataInterface $productMetadataInterface
	 *
	 * @internal param OrderFactory $orderFactory
	 */
	public function __construct(
		Context                  $context,
		Order                    $order,
		Registry                 $registry,
		StoreInterface           $store,
		UrlInterface             $urlBuilder,
		ScopeConfigInterface     $scopeConfig,
		Cart                     $cartHelper,
		ResponseFactory          $responseFactory,
		ProductMetadataInterface $productMetadataInterface

	) {
		parent::__construct($context);
		$this->registry = $registry;
		$this->order = $order;
		$this->store = $store;
		$this->urlBuilder = $urlBuilder;
		$this->scopeConfig = $scopeConfig;
		$this->cartHelper = $cartHelper;
		$this->responseFactory = $responseFactory;
		$this->productMetadataInterface = $productMetadataInterface;

		if($this->getOrderState($this->getOrderIdFromPost()) === \Magento\Sales\Model\Order::STATE_PENDING_PAYMENT) {

			$this->redirectToHomePage();
			die();
		}
	}

	/**
	 * @param string $orderId
	 *
	 * @return mixed
	 */
	public function getOrderState($orderId = '')
	{

		return $this->getOrderObject($orderId)->getState();
	}

	/**
	 * Render form with order information prepared for Payment Page
	 *
	 * @param string $id
	 *
	 * @return bool|\Imoje\Pbl\Model\Sales\Order
	 */
	public function getOrderObject($id = '')
	{
		if(empty($id)) {
			$id = $this->getOrderId();
		}

		return $this->order->loadOrderById($id);
	}

	/**
	 * @return integer
	 */
	public function getOrderId()
	{
		return $this->registry->registry('orderId');
	}

	/**
	 * @return string
	 */
	public function getOrderIdFromPost()
	{
		$id = '';
		if(isset($_POST['orderId'])) {
			$id = $_POST['orderId'];
		}

		return $id;
	}

	/**
	 * That function redirect to homepage
	 *
	 * @return mixed
	 */
	public function redirectToHomePage()
	{
		return $this->responseFactory->create()->setRedirect($this->urlBuilder->getUrl())->sendResponse(); //Redirect to cms page
	}

	/**
	 * @return array|string|void
	 */
	public function renderPbl()
	{

		$isPblPassed = $this->isPblPassed();

		if($isPblPassed['status']) {

			return $this->renderForm($this->getOrderId(), $isPblPassed['body']['pm'], $isPblPassed['body']['pmc']);
		}

		$writer = new Stream(BP . '/var/log/imoje.log');
		$this->logger = new Logger();
		$this->logger->addWriter($writer);

		$order = $this->getOrderObject($this->getOrderId());

		$imojeApi = new Api(
			$this->getConfigValue('payment/imoje_pbl/authorization_token'),
			$this->getConfigValue('payment/imoje_pbl/merchant_id'),
			$this->getConfigValue('payment/imoje_pbl/service_id'),
			$this->getConfigValue('payment/imoje_pbl/sandbox')
				? Util::ENVIRONMENT_SANDBOX
				: Util::ENVIRONMENT_PRODUCTION
		);

		$service = $imojeApi->getServiceInfo();

		if(!$service['success']) {

			$this->logger->info('Bad response in api, errors: '
				. $service['data']['body']);
			$this->redirectToHomePage();
			die();
		}

		if(!isset($service['body']['service']['isActive']) && $service['body']['service']['isActive']) {

			$this->logger->info('Service is inactive in imoje');
			$this->redirectToHomePage();
			die();
		}

		$currency = $order->getOrderCurrency()->getCode();
		$service = $service['body']['service'];

		$paymentMethodList = [];

		foreach($service['paymentMethods'] as $paymentMethod) {

			$pm = strtolower($paymentMethod['paymentMethod']);

			if($paymentMethod['isActive']
				&& $paymentMethod['isOnline']
				&& (
					($pm === Util::getPaymentMethod('pbl'))
					|| ($pm === Util::getPaymentMethod('ing'))
				)
				&& strtolower($paymentMethod['currency']) === strtolower($currency)

				// TODO: uncomment when api will be fine with limits

//                && (
//                    isset($paymentMethod['transactionLimits']['maxTransaction']['value'])
//                    && $paymentMethod['transactionLimits']['maxTransaction']['value']
//                )
//                && $paymentMethod['transactionLimits']['maxTransaction']['value'] > $cartTotal
//                && (
//                    isset($paymentMethod['transactionLimits']['minTransaction']['value'])
//                    && $paymentMethod['transactionLimits']['minTransaction']['value']
//                )
//                && $paymentMethod['transactionLimits']['minTransaction']['value'] < $cartTotal
			) {

				$paymentMethodList[] = [
					'paymentMethod'     => $paymentMethod['paymentMethod'],
					'paymentMethodCode' => $paymentMethod['paymentMethodCode'],
					'description'       => $paymentMethod['description'],
					'logo'              => Util::getPaymentMethodCodeLogo($paymentMethod['paymentMethodCode']),
				];
			}
		}

		return $paymentMethodList;
	}

	/**
	 * @return array
	 */
	public function isPblPassed()
	{
		$pm = $this->getRequest()->getParam('pm');
		$pmc = $this->getRequest()->getParam('pmc');

		if($pm && $pmc) {

			return [
				'status' => true,
				'body'   => [
					'pm'  => $this->getRequest()->getParam('pm'),
					'pmc' => $this->getRequest()->getParam('pmc'),
				],
			];
		}

		return [
			'status' => false,
			'body'   => [],
		];
	}

	/**
	 * @param string $orderId
	 *
	 * @return string
	 */
	public function renderForm($orderId, $pm = '', $pmc = '')
	{

		$imojeApi = new Api(
			$this->getConfigValue('payment/imoje_pbl/authorization_token'),
			$this->getConfigValue('payment/imoje_pbl/merchant_id'),
			$this->getConfigValue('payment/imoje_pbl/service_id'),
			$this->getConfigValue('payment/imoje_pbl/sandbox')
				? Util::ENVIRONMENT_SANDBOX
				: Util::ENVIRONMENT_PRODUCTION
		);


		$service = $imojeApi->getServiceInfo();

		$writer = new Stream(BP . '/var/log/imoje.log');
		$this->logger = new Logger();
		$this->logger->addWriter($writer);

		if(!$service['success']) {

			$this->logger->info('Bad response in api, errors: '
				. $service['data']['body']);
			$this->redirectToHomePage();
			die();
		}

		if(!isset($service['body']['service']['isActive']) && $service['body']['service']['isActive']) {

			$this->logger->info('Service is inactive in imoje');
			$this->redirectToHomePage();
			die();
		}

		$paymentMethodList = $service['body']['service']['paymentMethods'];

		$isValid = false;

		foreach($paymentMethodList as $paymentMethod) {

			if(($paymentMethod['paymentMethodCode'] === $pmc)
				&& ($paymentMethod['paymentMethod'] === $pm)
				&& $paymentMethod['isActive']
				&& $paymentMethod['isOnline']) {

				$isValid = true;
			}
		}

		if(!$isValid) {
			$this->logger->info(
				'Payment channel is inactive or offline or was not found. Code: ' . $pmc . ', Method: ' . $pm . '.'
			);
			$this->redirectToHomePage();
			die();
		}

		$order = $this->getOrderObject($orderId);

		$order->setState(\Magento\Sales\Model\Order::STATE_PENDING_PAYMENT);
		$order->save();

		$prepareData = $imojeApi->prepareData(
			Util::convertAmountToFractional($order->getGrandTotal()),
			$order->getOrderCurrency()->getCode(),
			$order->getIncrementId(),
			$pm,
			$pmc,
			$this->urlBuilder->getUrl('checkout/onepage/success/'),
			$this->urlBuilder->getUrl('checkout/onepage/failure/'),
			$order->getBillingAddress()->getFirstname(),
			$order->getBillingAddress()->getLastname(),
			$order->getCustomerEmail()
		);

		$transaction = $imojeApi->createTransaction($prepareData);

		if($transaction['success']) {
			return Util::createOrderForm(
				Api::parseStringToArray($transaction['body']),
				__('Continue'),
				$transaction['body']['action']['url']
			);
		}

		if(isset($transaction['data']['body']) && $transaction['data']['body']) {
			$this->logger->info('Transaction could not be initialized, error: ' . $transaction['data']['body']);
		}

		$this->redirectToHomePage();
		die();
	}

	/**
	 * @param string $value
	 *
	 * @return string
	 */
	private function getConfigValue($value)
	{
		return $this->scopeConfig->getValue($value, ScopeInterface::SCOPE_STORE);
	}
}
