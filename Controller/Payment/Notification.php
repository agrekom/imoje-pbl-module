<?php

namespace Imoje\Pbl\Controller\Payment;

include_once __DIR__ . "/../../../imoje-libs-module/PaymentCore/autoload.php";

use Exception;
use Imoje\Payment\Util;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender;
use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\Order\Payment\Transaction;
use Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface;
use Magento\Sales\Model\OrderRepository;
use Magento\Sales\Model\Service\InvoiceService;

/**
 * Class Notification
 *
 * @package Imoje\Pbl\Controller\Payment
 */
class Notification extends Action implements CsrfAwareActionInterface
{

	/**
	 * @var Order
	 */
	protected $order;

	/**
	 * @var ScopeConfigInterface
	 */
	protected $scopeConfig;

	/**
	 * @var BuilderInterface
	 */
	protected $builderInterface;

	/**
	 * @var OrderRepository
	 */
	protected $orderRepository;

	/**
	 * @var InvoiceService
	 */
	protected $invoiceService;

	/**
	 * @var \Magento\Framework\DB\Transaction
	 */
	protected $dbTransaction;

	/**
	 * @var Magento\Sales\Model\Order\Email\Sender\InvoiceSender
	 */
	protected $invoiceSender;

	/**
	 * ReturnUrl constructor.
	 *
	 * @param Context                           $context
	 * @param Order                             $order
	 * @param ScopeConfigInterface              $scopeConfig
	 * @param BuilderInterface                  $builderInterface
	 * @param OrderRepositoryInterface          $orderRepository
	 * @param InvoiceService                    $invoiceService
	 * @param \Magento\Framework\DB\Transaction $dbTransaction
	 * @param InvoiceSender                     $invoiceSender
	 */
	public function __construct(
		Context $context,
		Order $order,
		ScopeConfigInterface $scopeConfig,
		BuilderInterface $builderInterface,
		OrderRepositoryInterface $orderRepository,
		InvoiceService $invoiceService,
		\Magento\Framework\DB\Transaction $dbTransaction,
		InvoiceSender $invoiceSender

	) {
		parent::__construct($context);
		$this->order = $order;
		$this->scopeConfig = $scopeConfig;
		$this->builderInterface = $builderInterface;
		$this->orderRepository = $orderRepository;
		$this->invoiceService = $invoiceService;
		$this->dbTransaction = $dbTransaction;
		$this->invoiceSender = $invoiceSender;
	}

	/**
	 * @inheritDoc
	 */
	public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException
	{
		return null;
	}

	/**
	 * @inheritDoc
	 */
	public function validateForCsrf(RequestInterface $request): ?bool
	{
		return true;
	}

	/**
	 * @return ResponseInterface|ResultInterface|void
	 * @throws Exception
	 */
	public function execute()
	{

		// it can be order data or notification code - depends on verification notification
		$resultCheckRequestNotification = Util::checkRequestNotification($this->getConfigValue('payment/imoje_pbl/service_key'), $this->getConfigValue('payment/imoje_pbl/service_id'));
		$isDebugMode = $this->getConfigValue('payment/imoje_pbl/debug_mode');

		if(is_int($resultCheckRequestNotification)) {

			echo Util::notificationResponse(Util::NS_ERROR, $resultCheckRequestNotification, $isDebugMode);
			exit();
		}

		$transactionStatuses = Util::getTransactionStatuses();

		if(!isset($transactionStatuses[$resultCheckRequestNotification['transaction']['status']])) {
			echo Util::notificationResponse(Util::NS_ERROR, Util::NC_UNHANDLED_STATUS, $isDebugMode);
			exit();
		}

		$order = $this->order->loadByIncrementId($resultCheckRequestNotification['transaction']['orderId']);

		if($order->getEntityId() === null) {

			echo Util::notificationResponse(Util::NS_ERROR, Util::NC_ORDER_NOT_FOUND, $isDebugMode);
			exit();
		}

		if(!Util::checkRequestAmount($resultCheckRequestNotification, Util::convertAmountToFractional($order->getGrandTotal()), $order->getOrderCurrencyCode())) {

			echo Util::notificationResponse(Util::NS_ERROR, Util::NC_AMOUNT_NOT_MATCH, $isDebugMode);
			exit();
		}

		$currentOrderStatus = $order->getState();

		if($currentOrderStatus === Order::STATE_PROCESSING) {

			echo Util::notificationResponse(Util::NS_OK);
			exit();
		}

		switch($resultCheckRequestNotification['transaction']['status']) {
			case 'settled':
				$orderState = Order::STATE_PROCESSING;
				$order->addStatusToHistory($orderState, __('The payment has been accepted.'));
				$order->setTotalPaid(Util::convertAmountToMain($resultCheckRequestNotification['transaction']['amount']));
				$this->createTransaction($order, $resultCheckRequestNotification);
				$order->setState($orderState)->setStatus($orderState);
				$order->save();
				$this->createInvoice($order, $resultCheckRequestNotification['transaction']['id']);
				break;
			case 'rejected':
				$orderState = Order::STATE_PAYMENT_REVIEW;
				$order->setState($orderState)->setStatus($orderState);
				$order->save();
				$order->addStatusToHistory($orderState, __('The payment has been rejected.'));
				break;
			default:
				break;
		}

		echo Util::notificationResponse(Util::NS_OK);
		exit();
	}

	/**
	 * @param Order $order
	 * @param array $payloadDecoded
	 *
	 * @return bool
	 */
	public function createTransaction($order, $payloadDecoded)
	{
		//get payment object from order object
		//get the object of builder class
		if(
			!($payment = $order->getPayment())
			|| !($trans = $this->builderInterface)
		) {
			return false;
		}

		$transaction = $trans->setPayment($payment)
			->setOrder($order)
			->setTransactionId($payloadDecoded['transaction']['id'])
			->setFailSafe(true)
			//build method creates the transaction and returns the object
			->build(Transaction::TYPE_CAPTURE);
		if(!$transaction) {
			return false;
		}

		$payment->addTransactionCommentsToOrder(
			$transaction,
			__('Paid amount: %1.', Util::convertAmountToMain($payloadDecoded['transaction']['amount']))
		);

		$payment->setTransactionId($payloadDecoded['transaction']['id']);

		$payment->setParentTransactionId(null);

		try {
			$payment->save();
			$transaction->save();

			return true;
		} catch(Exception $e) {
			return false;
		}
	}

	/**
	 * @param Order  $order
	 * @param string $transactionId
	 *
	 * @return bool
	 */
	public function createInvoice($order, $transactionId)
	{

		if(!$order->canInvoice()) {

			return false;
		}

		$invoice = $order->prepareInvoice();
		$invoice->setRequestedCaptureCase(Invoice::CAPTURE_ONLINE);
		$invoice->setTransactionId($transactionId);
		$invoice->register();
		$invoice->pay();
		$invoice->save();

		if($invoice && !$invoice->getEmailSent()) {

			$this->invoiceSender->send($invoice);

			$order->addRelatedObject($invoice);
			$order->addStatusHistoryComment(__('You notified customer about invoice #%1.', $invoice->getIncrementId()))
				->setIsCustomerNotified(true)
				->save();

			return true;
		}

		return false;
	}

	/**
	 * @param string $value
	 *
	 * @return string
	 */
	private function getConfigValue($value)
	{
		return $this->scopeConfig->getValue($value, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}
}
