<?php

namespace Imoje\Pbl\Controller\Payment;

use Imoje\Pbl\Model\Order;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Checkout
 *
 * @package Imoje\Pbl\Controller\Payment
 */
class Checkout extends Action implements CsrfAwareActionInterface
{

	/**
	 * @var Order $order
	 */
	protected $order;

	/**
	 * @var PageFactory
	 */
	private $pageFactory;

	/**
	 * @var Registry
	 */
	private $registry;

	/**
	 * Pbl constructor.
	 *
	 * @param Context|Context $context
	 * @param Order           $order
	 * @param PageFactory     $pageFactory
	 * @param Registry        $registry
	 */
	public function __construct(Context $context, Order $order, PageFactory $pageFactory, Registry $registry)
	{
		parent::__construct($context);

		$this->order = $order;
		$this->pageFactory = $pageFactory;
		$this->registry = $registry;
	}

	/**
	 * @inheritDoc
	 */
	public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException
	{
		return null;
	}

	/**
	 * @inheritDoc
	 */
	public function validateForCsrf(RequestInterface $request): ?bool
	{
		return true;
	}

	/**
	 * Dispatch request
	 *
	 * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
	 * @throws \Magento\Framework\Exception\NotFoundException
	 */
	public function execute()
	{

		$this->registry->register('orderId', $this->order->getOrderId());

		return $this->pageFactory->create();
	}
}
